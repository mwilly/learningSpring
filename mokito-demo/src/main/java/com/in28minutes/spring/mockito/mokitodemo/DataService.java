package com.in28minutes.spring.mockito.mokitodemo;

public interface DataService {
    int[] retrieveAllData();
}

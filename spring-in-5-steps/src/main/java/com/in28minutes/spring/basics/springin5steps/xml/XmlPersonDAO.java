package com.in28minutes.spring.basics.springin5steps.xml;

public class XmlPersonDAO {

    XmlJdbcConnection xmlJdbcConnection;

    public XmlJdbcConnection getxmlJdbcConnection() {
        return xmlJdbcConnection;
    }

    public void setxmlJdbcConnection(XmlJdbcConnection jdbcConnection) {
        this.xmlJdbcConnection = jdbcConnection;
    }
}

package com.in28minutes.junit;

import static org.junit.Assert.*;

import org.junit.*;

public class MyMathTest {

    MyMath myMath = new MyMath();

    @Before
    public void before() {
        System.out.println("Before");
    }

    @After
    public void after() {
        System.out.println("After");
    }

    @BeforeClass
    public static void beforeClass() {
        System.out.println("Before Class");
    }

    @AfterClass
    public static void afterClass() {
        System.out.println("After Class");
    }

    @Test
    public void sum_with3numbers() {
        assertEquals(6,  myMath.sum(new int[]{1,2,3}));
        System.out.println("Test1");
    }

    @Test
    public void sum_with1number() {
        assertEquals(3, myMath.sum(new int[] { 3}));
        System.out.println("Test2");
    }
}
